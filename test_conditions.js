// Structure if - else
var compteur = 10
if (compteur < 10) {
    console.log('compteur est petit')
} else if (compteur < 20) {
    console.log('compteur est moyen')
} else {
    console.log('compteur est grand')
}


/* Structure switch case */
// Création d'une liste de mot
var arMots = ["Toto", "Titi", "Tutu"]

// Tirage d'un mot au hasard dans le tableau
var indexAleatoire = Math.floor(Math.random() * arMots.length)
var mot = arMots[indexAleatoire]

// Affichage en fonction du mot tiré
switch (mot.toLowerCase()) {
    case 'toto':
        console.log('Salut toto')
        break
    case 'titi':
        console.log('J\'ai cru voir Grosminet')
        break
    case 'tutu':
        console.log('Oh une danseuse')
        break
}

/* Test avec l'opérateur ternaire condition ? valeurSiVrai : valeurSiFaux */
var nbPerso = 1
var message1 = "Il y a " + nbPerso + " personne" + (nbPerso > 1 ? 's' : '')
console.log(message1)
nbPerso = 2
var message2 = "Il y a " + nbPerso + " personne" + (nbPerso > 1 ? 's' : '')
console.log(message2)