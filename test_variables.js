// Déclaration de variables
var compteur = 1
var texte = "Salut"
var nom = "John"
var booleen = true
var numTxt = "2"


// Affichage des types des variables 
console.log("compteur de type : " + typeof(compteur))
console.log("texte de type : " + typeof(texte))
console.log("booleen de type : " + typeof(booleen))
console.log("numTxt de type : " + typeof(numTxt))

// Tests 
console.log(compteur + 10)
console.log(numTxt + 15)

// Les chaînes sont concaténées
console.log(numTxt + compteur)

// Pour faire une opération, il faut convertir la chaîne en entier/réel
console.log(parseInt(numTxt) + compteur)